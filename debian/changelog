libtext-asciitable-perl (0.22-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libtext-asciitable-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 23:23:35 +0100

libtext-asciitable-perl (0.22-2) unstable; urgency=medium

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 28 Jun 2022 19:52:07 +0100

libtext-asciitable-perl (0.22-1) unstable; urgency=medium

  * Import upstream version 0.22
  * Update copyright years for upstream files

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 29 Dec 2016 17:27:09 +0100

libtext-asciitable-perl (0.21-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Import upstream version 0.21
  * Drop manpage-has-errors-from-pod2man.patch (applied upstream)
  * Update copyright years for debian/* packaging files
  * Bump Debhelper compat level to 9
  * Declare compliance with Debian policy 3.9.8
  * Add spelling-error-in-manpage.patch patch

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 29 Dec 2016 17:15:47 +0100

libtext-asciitable-perl (0.20-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * Add explicit Build-Depends on libmodule-build-perl
  * Update copyright years for debian/* packaging files
  * Wrap and sort fields in debian/control file
  * Declare compliance with Debian policy 3.9.6
  * Declare package as autopkgtestable
  * Add manpage-has-errors-from-pod2man.patch patch

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 05 Jun 2015 07:09:57 +0200

libtext-asciitable-perl (0.20-1) unstable; urgency=low

  * Imported Upstream version 0.20
    - Fix: "printing array of arrayrefs doesn't work" (Closes: #402906).
  * Drop fix-spelling-error-in-manpage.patch.
    Patch was applied upstream.

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 30 Nov 2011 07:04:31 +0100

libtext-asciitable-perl (0.19-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * Email change: Ansgar Burchardt -> ansgar@debian.org
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Imported Upstream version 0.19
  * Bump Debhelper compat level to 8.
    Use Build.PL by default.
    Adjust Build-Depends on debhelper to (>= 8) and remove
    libmodule-build-perl Build-Depends. Move perl from Build-Depends-Indep
    to Build-Depends for Module::Build use.
  * Add myself to Uploaders
  * Update debian/copyright file.
    Update Format to revision 174 of DEP5 proposal for machine-readable
    copyright file.
    Update debian/* packaging stanza
    Refer to Debian systems in general instead of only Debian GNU/Linux
    systems.
    Explicitly refer to GPL-1 license text in common-licenses.
  * Bump Standards-Version to 3.9.2
  * Convert to '3.0 (quilt)' source package format
  * Add fix-spelling-error-in-manpage.patch.
    Fix some spelling errors in manpage.

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 24 Nov 2011 19:21:06 +0100

libtext-asciitable-perl (0.18-2) unstable; urgency=low

  [ Ryan Niebur ]
  * Take over for the Debian Perl Group
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza);
    ${misc:Depends} to Depends: field. Changed: Maintainer set to Debian
    Perl Group <pkg-perl-maintainers@lists.alioth.debian.org> (was:
    Debian Catalyst Maintainers <pkg-catalyst-
    maintainers@lists.alioth.debian.org>); Debian Catalyst Maintainers
    <pkg-catalyst-maintainers@lists.alioth.debian.org> moved to
    Uploaders.
  * debian/watch: use dist-based URL.
  * Remove Florian Ragwitz from Uploaders (Closes: #523384)

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * Convert debian/copyright to proposed machine-readable format.
  * Refresh rules for debhelper 7.
  * Bump Standards-Version to 3.8.2.
  * Add myself to Uploaders.
  * No longer install README (same as POD documentation).

 -- Ansgar Burchardt <ansgar@43-1.org>  Mon, 20 Jul 2009 14:20:32 +0200

libtext-asciitable-perl (0.18-1) unstable; urgency=low

  * New upstream release
  * debian/watch: updated
  * debian/control:
   + improved description
   + Standards-Version: increased to 3.7.2.1
  * debian/compat: increased to 5

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Mon, 17 Jul 2006 12:16:44 +0200

libtext-asciitable-perl (0.17-2) unstable; urgency=low

  [Krzysztof Krzyzaniak (eloy)]
  * debian/control - Typo fixed, thanks to Martin Mikkelsen
    (closes: #340247)

  [ Florian Ragwitz ]
  * Removed an old maintainer address from debian/copyright. The old one
    doesn't work anymore.
  * Use my @debian.org address in the Uploaders field.

 -- Florian Ragwitz <rafl@debian.org>  Sun,  6 Nov 2005 17:39:13 +0100

libtext-asciitable-perl (0.17-1) unstable; urgency=low

  [Krzysztof Krzyzaniak (eloy)]
  * Added me to uploaders
  * Added debian/watch file

  [ Florian Ragwitz ]
  * Changed maintainer field to the pkg-catalyst group.
  * Added me to uploaders.
  * Install ansi-example.pl.

 -- Florian Ragwitz <rafl@debianforum.de>  Thu, 13 Oct 2005 16:07:37 +0200

libtext-asciitable-perl (0.15-1) unstable; urgency=low

  * Initial Release.

 -- Florian Ragwitz <florian@mookooh.org>  Mon, 30 May 2005 17:17:41 +0200
